module gitlab.com/bosi/decorder

go 1.20

require golang.org/x/tools v0.24.0

require (
	golang.org/x/mod v0.20.0 // indirect
	golang.org/x/sync v0.11.0 // indirect
)
